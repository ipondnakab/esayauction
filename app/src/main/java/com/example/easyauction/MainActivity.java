package com.example.easyauction;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    EditText txtEmailLogin, txtPasswordLogin;
    Button btnRegPage, btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        txtEmailLogin= findViewById(R.id.editTextEmailLogin);
        txtPasswordLogin = findViewById(R.id.editTextPasswordLogin);

        btnRegPage = findViewById(R.id.btnRegPage);
        btnLogin = findViewById(R.id.btnLogin);

        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), Home.class));
        }

        btnRegPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Register.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = txtEmailLogin.getText().toString().trim();
                String password = txtPasswordLogin.getText().toString().trim();

                if(TextUtils.isEmpty(email)){
                    txtEmailLogin.setError("Email is Required.");
                    return;
                }

                if(TextUtils.isEmpty(password)){
                    txtPasswordLogin.setError("Password is Required");
                    return;
                }

                if(password.length() < 6){
                    txtPasswordLogin.setError("Passwors must be 6 charactor");
                    return;
                }

                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(MainActivity.this, "Login Success!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), Home.class));
                        }else {
                            Toast.makeText(MainActivity.this, "Email or Password wrong!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });


    }
}
